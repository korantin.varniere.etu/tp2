export default class Router {
    static titleElement;
    static contentElement;
    static routes;

    static getTitleElement() {
        return this.titleElement;
    }
    static getContentElement() {
        return this.contentElement;
    }
    static getRoutes() {
        return this.routes;
    }
    
    static setTitleElement(titleElement) {
        this.titleElement = titleElement;
    }
    static setContentElement(contentElement) {
        this.contentElement = contentElement;
    }
    static setRoutes(routes) {
        this.routes = routes;
    }

    static navigate(path) {
        this.routes.forEach(element => {
            if (element.path === path) {
                this.titleElement.innerHTML = `<h1>${element.title}</h1>`;
                this.contentElement.innerHTML = element.page.render();
            }
        });
    }
}