import Component from "../components/Component.js";
import PizzaThumbnail from "../components/PizzaThumbnail.js";

export default class PizzaList extends Component {

    constructor(data) {
        super('section', {name: 'class', value: 'pizzaList'}, data.map(elt => new PizzaThumbnail(elt)));
    }

    set pizzas(data) {
        this.children = data.map(elt => new PizzaThumbnail(elt));
    }

}