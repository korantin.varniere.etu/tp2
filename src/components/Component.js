export default class Component {

	tagName;
	attribute;
	children;

	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}

	render() {
		return this.children == null ? `<${this.tagName} ${this.renderAttribute()}/>` : `<${this.tagName} ${this.renderAttribute()}>${this.renderChildren()}</${this.tagName}>`;
	}

	renderAttribute() {
		if (this.attribute == null) {
			return "";
		}
		return `${this.attribute.name} = "${this.attribute.value}"`; 
	}

	renderChildren() {
		if (this.children instanceof Array) {
			let res = "";
			this.children.forEach(element => {
				if (element instanceof Component) {
					res += element.render();
				} else {
					res += element;
				}
				res += " ";
			});
			return res;
		}
		return `${this.children}`;
	}

}